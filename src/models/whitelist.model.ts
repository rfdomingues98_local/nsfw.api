import { Model, DataTypes } from "sequelize";
import { database } from "../config/database";

export class Whitelist extends Model {
  public description!: string;
}

Whitelist.init(
  {
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
  },
  {
    tableName: "whitelist",
    sequelize: database,
    timestamps: false,
  }
);

Whitelist.sync().then(() => console.log('Whitelist table created'));
