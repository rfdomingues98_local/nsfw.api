import {Model, DataTypes } from "sequelize";
import {database} from "../config/database";

export interface LogInterface {
  image: string,
  touristId: string,
  userIp: string,
  description: string,
}

export class Log extends Model {
  public id!: string;
  public image!: string;
  public touristId!: string;
  public userIp!: string;
  public description!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Log.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    image: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    touristId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    userIp: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  },
  {
    tableName: "logs",
    sequelize: database, // this bit is important
  }
);

Log.sync().then(() => console.log("Logs table loaded."));