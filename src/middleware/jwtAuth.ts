import * as jwt from "jsonwebtoken";

const authenticateToken = (req, res, next) => {
	const authHeader = req.headers.authorization;
	const token = authHeader && authHeader.split(" ")[1];

	if (token == null) return res.sendStatus(401);

	jwt.verify(token, process.env.JWT_SECRET as string, (err: any, user: any) => {
		if (err) {
			console.log(err);
			res.sendStatus(403);
		}
		req.user = user;

		next();
	})
}

export default authenticateToken;