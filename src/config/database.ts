import {Sequelize} from "sequelize";

const db_name = process.env.DB_NAME || 'nsfwdb';
const db_user = process.env.DB_USER || 'root';
const db_password = process.env.DB_PASSWORD || '';
const db_host = process.env.DB_HOST || '127.0.0.1';
const db_port = parseInt(process.env.DB_PORT) || 3306;

export const database = new Sequelize(db_name, db_user, db_password, {
	logging: false,
  host: db_host,
  port: db_port,
  dialect: "mysql",
	dialectOptions: {
		
	},
  retry  : {
		match: [
			/ETIMEDOUT/,
			/EHOSTUNREACH/,
			/ECONNRESET/,
			/ECONNREFUSED/,
			/ETIMEDOUT/,
			/ESOCKETTIMEDOUT/,
			/EHOSTUNREACH/,
			/EPIPE/,
			/EAI_AGAIN/,
			/SequelizeConnectionError/,
			/SequelizeConnectionRefusedError/,
			/SequelizeHostNotFoundError/,
			/SequelizeHostNotReachableError/,
			/SequelizeInvalidConnectionError/,
			/SequelizeConnectionTimedOutError/
		],
		max  : 5
	}
});