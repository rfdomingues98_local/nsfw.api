import {Request, Response} from "express";
const jwt = require("jsonwebtoken");
import uploads from "./upload";
import {UploadController} from "../controllers/upload.controller";
import {LogController} from "../controllers/log.controller";
import authenticateToken from "../middleware/jwtAuth";

export class Routes {
	public uploadController: UploadController = new UploadController();
	public logController: LogController = new LogController();
  
	public routes(app): void {
    app.route("/")
			.get(authenticateToken, (req: Request, res: Response) => {
      	res.json({"access": "granted"});
    	})
			.post(authenticateToken, uploads.any(), this.uploadController.index);
		
		app.route("/get/:id")
			.get(authenticateToken, this.logController.index);
	}
}