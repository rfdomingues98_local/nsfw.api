import * as express from "express";
import * as bodyParser from "body-parser";
import {Routes} from "./config/routes";

class App {
	public app: express.Application;
	public routePrv: Routes = new Routes();
	
	constructor() {
		this.app = express();
		this.config();
		this.routePrv.routes(this.app);
	}

	private config(): void {
    this.app.use(bodyParser.json({limit: '50mb'}));
    this.app.use(bodyParser.urlencoded({limit: '50mb', extended: false}));
  }
}

export default new App().app;