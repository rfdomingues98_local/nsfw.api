import { Whitelist } from "../models/whitelist";

export class WhitelistController {
  public getWhitelist() {
    return Whitelist.findAll();
  }
}
