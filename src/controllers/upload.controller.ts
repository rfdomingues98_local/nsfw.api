import { Request, Response } from 'express';
import * as AWS from 'aws-sdk';
import { LogController } from './log.controller';
import * as fs from 'fs';
import { WhitelistController } from './whitelist.controller';
require('dotenv/config');

const config = new AWS.Config({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});

const client = new AWS.Rekognition();

interface categoryInterface {
  Name: string;
  Confidence: number;
}
export class UploadController {
  public async index(req: Request, res: Response) {
    const whitelistController: WhitelistController = new WhitelistController();
    const whitelist = await whitelistController.getWhitelist();
    const allowList = whitelist.map((item) => item.description);

    let result = [];
    if (!req.files) {
      res.status(400).send('Missing image.');
    } else {
      for (let img in req.files) {
        const params = {
          Image: {
            Bytes: req.files[img].buffer,
          },
        };
        try {
          const response = await client
            .detectModerationLabels(params)
            .promise();

          if (response.ModerationLabels.length != 0) {
            const categories: Array<categoryInterface> = [];
            response.ModerationLabels.filter((el) => el.ParentName != '').map(
              (el) => {
                let category: categoryInterface = {
                  Name: el.Name,
                  Confidence: el.Confidence,
                };
                categories.push(category);
              }
            );
            if (categories.some((el) => !allowList.includes(el.Name))) {
              const logController: LogController = new LogController();
              fs.writeFileSync(
                __dirname + '/../../uploads/' + req.files[img].originalname,
                req.files[img].buffer
              );
              const filteredCategories = categories.filter(
                (el) => !allowList.includes(el.Name)
              );
              req.body.image = req.files[img].originalname;
              req.body.userIp = req.socket.remoteAddress;
              let description = 'Failed due to: ';
              for (let i = 0; i < filteredCategories.length; i++) {
                description += `${
                  filteredCategories[i].Name
                } with a confidence of ${filteredCategories[
                  i
                ].Confidence.toFixed(3)}`;
                if (i == filteredCategories.length - 1) description += '.';
                else description += ', ';
              }
              req.body.description = description;
              const logId = await logController.create(req, res);
              result.push({
                fieldname: req.files[img].fieldname,
                success: false,
                logId,
                labels: filteredCategories,
                description
              });
              console.log(JSON.stringify(result));
            } else {
              result.push({
                fieldname: req.files[img].fieldname,
                success: true,
              });
            }
          } else {
            result.push({ fieldname: req.files[img].fieldname, success: true });
          }
        } catch (err) {
          console.log(err, err.stack);
          res.status(500).json(err);
        }
      }
      res.json(result);
    }
  }
}
