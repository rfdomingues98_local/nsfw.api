import {Request, Response} from "express";
import {Log, LogInterface} from "../models/log.model";

export class LogController {
	public index(req: Request, res: Response) {
		Log.findAll<Log>({where: {id: req.params.id}})
			.then((log: Array<Log>) => res.json(log))
			.catch((err: Error) => res.status(500).json(err));
	}

	public async create(req: Request, res: Response) {
		const params: LogInterface = req.body;
		return Log.create<Log>(params)
			.then((log: Log) => (log.id))
			.catch((err: Error) => console.log(err));
	}
}